package com.androidbyexample.intents

import android.view.View
import androidx.appcompat.app.AppCompatActivity

abstract class MyActivityBase: AppCompatActivity() {
    protected fun Int.onClick(handler : () -> Unit) =
        findViewById<View>(this).setOnClickListener { handler() }
}
