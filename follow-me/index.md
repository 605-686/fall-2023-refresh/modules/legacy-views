---
title: Legacy Views
---

We've talked about Jetpack Compose for the entire class, but it's likely that you'll probably see code that uses the old "views" system.

In this module, we'll explore view-based user interfaces. The videos contained here are from an older (Summer 2020) face-to-face term, before I converted the content to use Jetpack Compose. I started to write textual content but realized it would be more valuable to make the older videos available. Note that this is a lot of content, but you can select topics you're interested in from the playlist.

!!! Note

    None of this content will be used in any assignments; it is only here for your information.

!!! Note

    The "Room" videos in this batch are similar in content to the Room material on this site,
    but use `LiveData` for asynchronous data fetching rather than Kotlin `Flow`.


Video Playlist: [https://www.youtube.com/playlist?list=PLW-6wqFEcgTpc2rBCGhyFNksqaG0OhlvJ](https://www.youtube.com/playlist?list=PLW-6wqFEcgTpc2rBCGhyFNksqaG0OhlvJ)

Sample Code: [https://gitlab.com/605-686/fall-2023-refresh/modules/legacy-views](https://gitlab.com/605-686/fall-2023-refresh/modules/legacy-views)
