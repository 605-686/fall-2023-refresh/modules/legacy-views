package com.androidbyexample.movies.nav.starter.of.doom


import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.androidbyexample.movies.nav.starter.of.doom.R

/**
 * A simple [Fragment] subclass.
 */
class ActorFragment : BaseFragment(R.layout.fragment_actor) {
    private val args : com.androidbyexample.movies.nav.starter.of.doom.ActorFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.text).text = args.actorId
    }
}
