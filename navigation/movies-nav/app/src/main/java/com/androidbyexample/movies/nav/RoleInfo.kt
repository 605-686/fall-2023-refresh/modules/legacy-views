package com.androidbyexample.movies.nav

import com.androidbyexample.movies.nav.HasId

data class RoleInfo(
    override val id : String,
    val actorId : String,
    val actorName : String,
    val roleName : String,
    val order : Int
) : HasId