package com.androidbyexample.movies.nav

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.androidbyexample.movies.nav.HasId
import java.util.*

@Entity
data class Movie(
    @PrimaryKey override var id : String = UUID.randomUUID().toString(),
    var title : String = "",
    var description : String = "") : HasId
