package com.androidbyexample.movies.nav

interface HasId {
    val id : String
}