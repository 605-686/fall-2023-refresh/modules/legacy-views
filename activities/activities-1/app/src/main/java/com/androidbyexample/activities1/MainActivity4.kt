package com.androidbyexample.activities1

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

// NOTE: Added a new coroutine version of this example
class MainActivity4 : AppCompatActivity() {
    private var n = 1
    private var textView : TextView? = null

    // NOTE: OnLifecycleEvent annotation deprecated (uses reflection)
    //       Changed to extend DefaultLifecycleObserver

    inner class Updater : DefaultLifecycleObserver {
        private var job: Job? = null
        override fun onStart(owner: LifecycleOwner) {
            job?.cancel()
            job = owner.lifecycleScope.launch(Dispatchers.Default) {
                while (true) {
                    delay(1000)
                    n++
                    withContext(Dispatchers.Main) {
                        textView?.text = n.toString()
                    }
                    Log.d("!!", "count=$n")
                }
            }
        }

        override fun onStop(owner: LifecycleOwner) {
            job?.cancel()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("!!", "onCreate3")
        setContentView(R.layout.activity_main)

        lifecycle.addObserver(Updater())

        textView = findViewById(R.id.textView)
        val button = findViewById<Button>(R.id.button)

        textView?.text = n.toString()
        button.setOnClickListener {
            n++
            textView?.text = n.toString()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("!!", "onResume3")
    }

    override fun onStart() {
        super.onStart()
        Log.d("!!", "onStart3")
    }

    override fun onPause() {
        Log.d("!!", "onPause3")
        super.onPause()
    }
    override fun onStop() {
        Log.d("!!", "onStop3")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d("!!", "onDestroy3")
        super.onDestroy()
    }
}
