package com.androidbyexample.activities1

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

class MainActivity3 : AppCompatActivity() {
    private var n = 1
    private var textView : TextView? = null

    // NOTE: OnLifecycleEvent annotation deprecated (uses reflection)
    //       Changed to extend DefaultLifecycleObserver
    inner class Updater : DefaultLifecycleObserver {
        private var counter : Counter? = null

        override fun onStart(owner: LifecycleOwner) {
            counter = Counter()
            counter?.start()
        }

        override fun onStop(owner: LifecycleOwner) {
            counter?.interrupt()
            counter = null
        }
    }

    inner class Counter : Thread() {
        override fun run() {
            while (!isInterrupted) {
                try {
                    sleep(1000)
                } catch (e : InterruptedException) {
                    interrupt()
                }
                n++
                runOnUiThread {
                    textView?.text = n.toString()
                }
                Log.d("!!", "count=$n")
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("!!", "onCreate3")
        setContentView(R.layout.activity_main)

        lifecycle.addObserver(Updater())

        textView = findViewById(R.id.textView)
        val button = findViewById<Button>(R.id.button)

        textView?.text = n.toString()
        button.setOnClickListener {
            n++
            textView?.text = n.toString()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("!!", "onResume3")
    }

    override fun onStart() {
        super.onStart()
        Log.d("!!", "onStart3")
    }

    override fun onPause() {
        Log.d("!!", "onPause3")
        super.onPause()
    }
    override fun onStop() {
        Log.d("!!", "onStop3")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d("!!", "onDestroy3")
        super.onDestroy()
    }
}
