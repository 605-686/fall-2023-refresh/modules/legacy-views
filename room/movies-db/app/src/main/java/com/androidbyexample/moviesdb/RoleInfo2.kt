package com.androidbyexample.moviesdb

import androidx.room.Relation
import com.androidbyexample.moviesdb.Actor

data class RoleInfo2(
    @Relation(parentColumn = "actorId", entityColumn = "id")
    var actor : Actor,
    var movieId : String,
    var actorId : String,
    var roleName : String,
    var order : Int
)