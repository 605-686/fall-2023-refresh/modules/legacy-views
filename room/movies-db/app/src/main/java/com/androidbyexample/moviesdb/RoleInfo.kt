package com.androidbyexample.moviesdb

import androidx.room.Embedded
import com.androidbyexample.moviesdb.Actor

data class RoleInfo(
    @Embedded val actor : Actor,
    val roleName : String,
    val order : Int
)