package com.androidbyexample.movies.recyclerview

import androidx.room.Relation
import com.androidbyexample.movies.recyclerview.Actor

data class RoleInfo2(
    @Relation(parentColumn = "actorId", entityColumn = "id")
    var actor : Actor,
    var movieId : String,
    var actorId : String,
    var roleName : String,
    var order : Int
)