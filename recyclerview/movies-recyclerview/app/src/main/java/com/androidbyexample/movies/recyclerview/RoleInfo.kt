package com.androidbyexample.movies.recyclerview

import androidx.room.Embedded
import com.androidbyexample.movies.recyclerview.Actor

data class RoleInfo(
    @Embedded val actor : Actor,
    val roleName : String,
    val order : Int
)